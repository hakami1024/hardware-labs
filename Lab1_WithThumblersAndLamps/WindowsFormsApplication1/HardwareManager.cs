﻿using System;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class HardwareManager
    {
        private SerialPort port;
        Timer t;
    
        bool[] lampStates;
        bool[] thumbStates;
        private string lastInput = "";

        public bool ChangeLampState( int i ){
            return lampStates[ i ] = !lampStates[ i ];
        }

        public bool GetLampState( int i ) {
            return lampStates[ i ];
        }

        public bool ChangeThumbState( int i ) {
            return thumbStates[ i ] = !thumbStates[ i ];
        }

        public bool GetThumbState( int i ) {
            return thumbStates[ i ];
        }

        public event Action LampStatesChanged, ThumbStatesChanged;
        bool successInit = false;

        internal HardwareManager( int lampNum, int thumbNum, Action lampsChange, Action thumbsChange ){
            lampStates = new bool[lampNum];
            thumbStates = new bool[thumbNum];
            
            System.ComponentModel.IContainer components = new System.ComponentModel.Container();
            Application.ApplicationExit += onAppExit;
            
            try{
                port = new SerialPort( components );
                //port.BaudRate = 9600;
                port.Open();
                successInit = true;
            }catch( Exception ex){
                MessageBox.Show( ex.ToString() );
            }
            

            LampStatesChanged = lampsChange;
            ThumbStatesChanged = thumbsChange;

            t = new Timer();
            t.Interval = 1000;
            t.Tick += SetStates;
            //if( successInit)
                t.Start();
        }

        public void SendStates(){
            int value = lampStates.Aggregate( 0, ( current, t ) => current << 1 | ( t ? 1 : 0 ) );
            //MessageBox.Show( Convert.ToString( value, 2 ) + "#0400" + value.ToString( "X" ) + '\r' );
            if( successInit )
                port.Write( "#0400"+value.ToString("X")+'\r' );
        }

        private int debug = 0;
        public void SetStates( Object obj, EventArgs args ){
            //MessageBox.Show( "in SetStates" );
            String input;
            if( successInit ){
                port.Write( "$046\r" );
                input = port.ReadTo( "\r" );
            }
            else
                if( debug % 4 == 0 )
                    input = "!FFFF00\r";
                else if ( debug % 4 == 1 )
                    input = "!FF0000\r";
                else if ( debug % 4 == 2 )
                    input = "!00FF00\r";
                else //if ( debug % 4 == 3 )
                    input = "!000000\r";
            debug++;

            if( input == lastInput )
                return;
            lastInput = input;

            int lampValues = int.Parse( input.Substring( 1, 2 ), NumberStyles.HexNumber );
           
            for(int i=0; i<lampStates.Length; i++){
                lampStates[i] = (lampValues & 1) != 0 ;
                lampValues = lampValues >> 1;
            }

            LampStatesChanged();

            int thumbValues = int.Parse( input.Substring( 3, 2 ), NumberStyles.HexNumber );
            for ( int i = 0; i < thumbStates.Length; i++ ) {
                thumbStates[ i ] = ( thumbValues & 1 ) != 0;
                thumbValues >>= 1;
            }

            ThumbStatesChanged();
        }

        private void onAppExit( Object o, EventArgs e ){
            port.Close();
            t.Stop();
        }
    }
}
