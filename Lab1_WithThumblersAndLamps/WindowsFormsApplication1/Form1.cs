﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        private readonly Button[] lampButtons;
        private readonly Button[] thumbButtons;

        private readonly HardwareManager manager;

        public Form1() {
            InitializeComponent();
            lampButtons = new[]{
                lamp_button1, lamp_button2, lamp_button3, 
                lamp_button4, lamp_button5, lamp_button6, 
                lamp_button7, lamp_button8 };
            thumbButtons = new[]{
                thumb_button1, thumb_button2, thumb_button3, 
                thumb_button4, thumb_button5, thumb_button6, 
                thumb_button7, thumb_button8 };
            manager = new HardwareManager( lampButtons.Length, thumbButtons.Length, updateLamps, updateThumbs );
        }

        private void lampButton_Click( object sender, EventArgs e ){
            int i = Array.IndexOf( lampButtons, (Button)sender );
            if( i != -1 ){
                lampButtons[ i ].Image = manager.ChangeLampState( i )
                    ? Properties.Resources.lamp_on
                    : Properties.Resources.lamp_off;
                manager.SendStates();
            }

            
        }

        private void thumpButton_Click( object sender, EventArgs e ){
           /* int i = Array.IndexOf( thumbButtons, ( Button )sender );
            if( i != -1)
                thumbButtons[i].Text = manager.ChangeThumbState( i ) ? "On" : "Off";
            */
        }

        private void updateLamps(){
            for( int i=0; i<lampButtons.Length; i++ ){
                lampButtons[ i ].Image = manager.GetLampState( i )
                    ? Properties.Resources.lamp_on
                    : Properties.Resources.lamp_off;
            }
        }

        private void updateThumbs(){
            for ( int i = 0; i < lampButtons.Length; i++ ) {
                thumbButtons[ i ].Text = manager.GetThumbState( i ) ? "On" : "Off";
            }
        }
    }
}
