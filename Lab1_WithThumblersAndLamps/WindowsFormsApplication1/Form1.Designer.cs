﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose( bool disposing ) {
            if ( disposing && ( components != null ) ) {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lamp_button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.thumb_button1 = new System.Windows.Forms.Button();
            this.thumb_button2 = new System.Windows.Forms.Button();
            this.thumb_button3 = new System.Windows.Forms.Button();
            this.thumb_button4 = new System.Windows.Forms.Button();
            this.thumb_button5 = new System.Windows.Forms.Button();
            this.thumb_button6 = new System.Windows.Forms.Button();
            this.thumb_button7 = new System.Windows.Forms.Button();
            this.lamp_button2 = new System.Windows.Forms.Button();
            this.lamp_button3 = new System.Windows.Forms.Button();
            this.lamp_button4 = new System.Windows.Forms.Button();
            this.lamp_button7 = new System.Windows.Forms.Button();
            this.lamp_button6 = new System.Windows.Forms.Button();
            this.lamp_button5 = new System.Windows.Forms.Button();
            this.lamp_button8 = new System.Windows.Forms.Button();
            this.thumb_button8 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Lamps:";
            // 
            // lamp_button1
            // 
            this.lamp_button1.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button1.Image" ) ) );
            this.lamp_button1.Location = new System.Drawing.Point( 12, 29 );
            this.lamp_button1.Name = "lamp_button1";
            this.lamp_button1.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button1.TabIndex = 0;
            this.lamp_button1.UseVisualStyleBackColor = true;
            this.lamp_button1.Click += new System.EventHandler( this.lampButton_Click );
            // 
            // lamp_button2
            // 
            this.lamp_button2.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button2.Image" ) ) );
            this.lamp_button2.Location = new System.Drawing.Point( 93, 29 );
            this.lamp_button2.Name = "lamp_button2";
            this.lamp_button2.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button2.TabIndex = 16;
            this.lamp_button2.UseVisualStyleBackColor = true;
            this.lamp_button2.Click += new System.EventHandler( this.lampButton_Click );

            // 
            // lamp_button3
            // 
            this.lamp_button3.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button3.Image" ) ) );
            this.lamp_button3.Location = new System.Drawing.Point( 174, 29 );
            this.lamp_button3.Name = "lamp_button3";
            this.lamp_button3.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button3.TabIndex = 17;
            this.lamp_button3.UseVisualStyleBackColor = true;
            this.lamp_button3.Click += new System.EventHandler( this.lampButton_Click );

            // 
            // lamp_button4
            // 
            this.lamp_button4.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button4.Image" ) ) );
            this.lamp_button4.Location = new System.Drawing.Point( 255, 29 );
            this.lamp_button4.Name = "lamp_button4";
            this.lamp_button4.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button4.TabIndex = 18;
            this.lamp_button4.UseVisualStyleBackColor = true;
            this.lamp_button4.Click += new System.EventHandler( this.lampButton_Click );

            // 
            // lamp_button5
            // 
            this.lamp_button5.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button5.Image" ) ) );
            this.lamp_button5.Location = new System.Drawing.Point( 336, 29 );
            this.lamp_button5.Name = "lamp_button5";
            this.lamp_button5.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button5.TabIndex = 19;
            this.lamp_button5.UseVisualStyleBackColor = true;
            this.lamp_button5.Click += new System.EventHandler( this.lampButton_Click );

            // 
            // lamp_button6
            // 
            this.lamp_button6.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button6.Image" ) ) );
            this.lamp_button6.Location = new System.Drawing.Point( 417, 29 );
            this.lamp_button6.Name = "lamp_button6";
            this.lamp_button6.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button6.TabIndex = 20;
            this.lamp_button6.UseVisualStyleBackColor = true;
            this.lamp_button6.Click += new System.EventHandler( this.lampButton_Click );

            // 
            // lamp_button7
            // 
            this.lamp_button7.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button7.Image" ) ) );
            this.lamp_button7.Location = new System.Drawing.Point( 498, 29 );
            this.lamp_button7.Name = "lamp_button7";
            this.lamp_button7.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button7.TabIndex = 21;
            this.lamp_button7.UseVisualStyleBackColor = true;
            this.lamp_button7.Click += new System.EventHandler( this.lampButton_Click );

            // 
            // lamp_button8
            // 
            this.lamp_button8.Image = ( ( System.Drawing.Image )( resources.GetObject( "lamp_button8.Image" ) ) );
            this.lamp_button8.Location = new System.Drawing.Point( 579, 29 );
            this.lamp_button8.Name = "lamp_button8";
            this.lamp_button8.Size = new System.Drawing.Size( 75, 101 );
            this.lamp_button8.TabIndex = 22;
            this.lamp_button8.UseVisualStyleBackColor = true;
            this.lamp_button8.Click += new System.EventHandler( this.lampButton_Click );

            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Thumblers:";
            // 
            // thumb_button1
            // 
            this.thumb_button1.Location = new System.Drawing.Point(12, 153);
            this.thumb_button1.Name = "thumb_button1";
            this.thumb_button1.Size = new System.Drawing.Size(75, 23);
            this.thumb_button1.TabIndex = 9;
            this.thumb_button1.Text = "On";
            this.thumb_button1.UseVisualStyleBackColor = true;
            this.thumb_button1.Click += new System.EventHandler( this.thumpButton_Click );
            // 
            // thumb_button2
            // 
            this.thumb_button2.Location = new System.Drawing.Point(93, 153);
            this.thumb_button2.Name = "thumb_button2";
            this.thumb_button2.Size = new System.Drawing.Size(75, 23);
            this.thumb_button2.TabIndex = 10;
            this.thumb_button2.Text = "On";
            this.thumb_button2.UseVisualStyleBackColor = true;
            this.thumb_button2.Click += new System.EventHandler( this.thumpButton_Click );

            // 
            // thumb_button3
            // 
            this.thumb_button3.Location = new System.Drawing.Point(174, 153);
            this.thumb_button3.Name = "thumb_button3";
            this.thumb_button3.Size = new System.Drawing.Size(75, 23);
            this.thumb_button3.TabIndex = 11;
            this.thumb_button3.Text = "On";
            this.thumb_button3.UseVisualStyleBackColor = true;
            this.thumb_button3.Click += new System.EventHandler( this.thumpButton_Click );

            // 
            // thumb_button4
            // 
            this.thumb_button4.Location = new System.Drawing.Point(255, 153);
            this.thumb_button4.Name = "thumb_button4";
            this.thumb_button4.Size = new System.Drawing.Size(75, 23);
            this.thumb_button4.TabIndex = 12;
            this.thumb_button4.Text = "On";
            this.thumb_button4.UseVisualStyleBackColor = true;
            this.thumb_button4.Click += new System.EventHandler( this.thumpButton_Click );

            // 
            // thumb_button5
            // 
            this.thumb_button5.Location = new System.Drawing.Point(336, 153);
            this.thumb_button5.Name = "thumb_button5";
            this.thumb_button5.Size = new System.Drawing.Size(75, 23);
            this.thumb_button5.TabIndex = 13;
            this.thumb_button5.Text = "On";
            this.thumb_button5.UseVisualStyleBackColor = true;
            this.thumb_button5.Click += new System.EventHandler( this.thumpButton_Click );

            // 
            // thumb_button6
            // 
            this.thumb_button6.Location = new System.Drawing.Point(417, 153);
            this.thumb_button6.Name = "thumb_button6";
            this.thumb_button6.Size = new System.Drawing.Size(75, 23);
            this.thumb_button6.TabIndex = 14;
            this.thumb_button6.Text = "On";
            this.thumb_button6.UseVisualStyleBackColor = true;
            this.thumb_button6.Click += new System.EventHandler( this.thumpButton_Click );

            // 
            // thumb_button7
            // 
            this.thumb_button7.Location = new System.Drawing.Point(498, 153);
            this.thumb_button7.Name = "thumb_button7";
            this.thumb_button7.Size = new System.Drawing.Size(75, 23);
            this.thumb_button7.TabIndex = 15;
            this.thumb_button7.Text = "On";
            this.thumb_button7.UseVisualStyleBackColor = true;
            this.thumb_button7.Click += new System.EventHandler( this.thumpButton_Click );

            
            
            // 
            // thumb_button8
            // 
            this.thumb_button8.Location = new System.Drawing.Point(579, 153);
            this.thumb_button8.Name = "thumb_button8";
            this.thumb_button8.Size = new System.Drawing.Size(75, 23);
            this.thumb_button8.TabIndex = 23;
            this.thumb_button8.Text = "On";
            this.thumb_button8.UseVisualStyleBackColor = true;
            this.thumb_button8.Click += new System.EventHandler( this.thumpButton_Click );

            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(697, 206);
            this.Controls.Add(this.thumb_button8);
            this.Controls.Add(this.lamp_button8);
            this.Controls.Add(this.lamp_button7);
            this.Controls.Add(this.lamp_button6);
            this.Controls.Add(this.lamp_button5);
            this.Controls.Add(this.lamp_button4);
            this.Controls.Add(this.lamp_button3);
            this.Controls.Add(this.lamp_button2);
            this.Controls.Add(this.thumb_button7);
            this.Controls.Add(this.thumb_button6);
            this.Controls.Add(this.thumb_button5);
            this.Controls.Add(this.thumb_button4);
            this.Controls.Add(this.thumb_button3);
            this.Controls.Add(this.thumb_button2);
            this.Controls.Add(this.thumb_button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lamp_button1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "Form1";
            this.Text = "Lab2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button lamp_button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button thumb_button1;
        private System.Windows.Forms.Button thumb_button2;
        private System.Windows.Forms.Button thumb_button3;
        private System.Windows.Forms.Button thumb_button4;
        private System.Windows.Forms.Button thumb_button5;
        private System.Windows.Forms.Button thumb_button6;
        private System.Windows.Forms.Button thumb_button7;
        private System.Windows.Forms.Button lamp_button2;
        private System.Windows.Forms.Button lamp_button3;
        private System.Windows.Forms.Button lamp_button4;
        private System.Windows.Forms.Button lamp_button7;
        private System.Windows.Forms.Button lamp_button6;
        private System.Windows.Forms.Button lamp_button5;
        private System.Windows.Forms.Button lamp_button8;
        private System.Windows.Forms.Button thumb_button8;
    }
}

