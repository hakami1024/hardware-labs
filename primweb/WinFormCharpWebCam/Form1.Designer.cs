﻿namespace WinFormCharpWebCam
{
    partial class MainWinForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgVideo = new System.Windows.Forms.PictureBox();
            this.intervalLabel = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.nUDTimeValue = new System.Windows.Forms.NumericUpDown();
            this.dUDTimeSign = new System.Windows.Forms.DomainUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDTimeValue)).BeginInit();
            this.SuspendLayout();
            // 
            // imgVideo
            // 
            this.imgVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgVideo.Location = new System.Drawing.Point(55, 41);
            this.imgVideo.Name = "imgVideo";
            this.imgVideo.Size = new System.Drawing.Size(347, 280);
            this.imgVideo.TabIndex = 0;
            this.imgVideo.TabStop = false;
            // 
            // intervalLabel
            // 
            this.intervalLabel.AutoSize = true;
            this.intervalLabel.Location = new System.Drawing.Point(52, 336);
            this.intervalLabel.Name = "intervalLabel";
            this.intervalLabel.Size = new System.Drawing.Size(92, 13);
            this.intervalLabel.TabIndex = 6;
            this.intervalLabel.Text = "Capturing interval:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(364, 331);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(103, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start capturing";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // nUDTimeValue
            // 
            this.nUDTimeValue.Location = new System.Drawing.Point(147, 334);
            this.nUDTimeValue.Maximum = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            this.nUDTimeValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDTimeValue.Name = "nUDTimeValue";
            this.nUDTimeValue.Size = new System.Drawing.Size(72, 20);
            this.nUDTimeValue.TabIndex = 8;
            this.nUDTimeValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nUDTimeValue.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // dUDTimeSign
            // 
            this.dUDTimeSign.Items.Add("milliseconds");
            this.dUDTimeSign.Items.Add("seconds");
            this.dUDTimeSign.Items.Add("minutes");
            this.dUDTimeSign.Location = new System.Drawing.Point(225, 334);
            this.dUDTimeSign.Name = "dUDTimeSign";
            this.dUDTimeSign.ReadOnly = true;
            this.dUDTimeSign.Size = new System.Drawing.Size(120, 20);
            this.dUDTimeSign.TabIndex = 9;
            this.dUDTimeSign.Text = "milliseconds";
            this.dUDTimeSign.SelectedItemChanged += new System.EventHandler(this.dUDTimeSign_SelectedItemChanged);
            // 
            // MainWinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 362);
            this.Controls.Add(this.dUDTimeSign);
            this.Controls.Add(this.nUDTimeValue);
            this.Controls.Add(this.intervalLabel);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.imgVideo);
            this.Name = "MainWinForm";
            this.Text = "WebCam";
            this.Load += new System.EventHandler(this.mainWinForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDTimeValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgVideo;
        private System.Windows.Forms.Label intervalLabel;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.NumericUpDown nUDTimeValue;
        private System.Windows.Forms.DomainUpDown dUDTimeSign;
    }
}

