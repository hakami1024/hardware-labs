﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using WebCam_Capture;
using Timer = System.Windows.Forms.Timer;

namespace WinFormCharpWebCam
{
    class WebCam
    {
        private PictureBox frameImage;
        private readonly Timer myTimer = new Timer();
        private int savingTimes;
        private const int FRAME_NUM = 10;
        private Action<bool> finishMethod;
        private String dir = "";
        private Thread dirChooseThread;

        public void InitializeWebCam(ref PictureBox imageControl)
        {
            this.frameImage = imageControl;
            WebCamCapture webcam = new WebCamCapture { FrameNumber = 0ul, TimeToCapture_milliseconds = FRAME_NUM };
            webcam.ImageCaptured += webcam_ImageCaptured;
            webcam.Start( 0 );
            
            myTimer.Tick += makeCapture;
            Application.ApplicationExit += appClosing;
        }

        void webcam_ImageCaptured(object source, WebcamEventArgs e)
        {
            this.frameImage.Invoke( (Action)( () =>{
                this.frameImage.Image = e.WebCamImage;
            }) );

        }

        private void start(){
            savingTimes = 0;
            ProcessStartInfo pfi = new ProcessStartInfo( "Explorer.exe", "/root, " + dir );
            Process.Start( pfi );
            myTimer.Start();
        }

        public void Start( int period, Action<bool> onFinishing){
            this.finishMethod = onFinishing;
            myTimer.Interval = period;

            dirChooseThread = new Thread( () =>
            {
                try{
                    FolderBrowserDialog s = new FolderBrowserDialog{
                        Description = "Choose directory for the photos."
                    };
                    DialogResult res = s.ShowDialog();
                    if( res != DialogResult.OK )
                        finishMethod( false );
                    else{
                        dir = s.SelectedPath;
                        frameImage.Invoke( ( Action )start );
                    }
                }
                catch( ThreadAbortException ){}

            } ){ IsBackground = false };

            dirChooseThread.SetApartmentState( ApartmentState.STA );
            dirChooseThread.Start();
        }

        private void makeCapture(Object o, EventArgs e){
            Image inputImage = this.frameImage.Image;

            String fileDir = dir + "\\Image_" + ( savingTimes + 1 ) + ".jpg" ;
            FileStream fstream = new FileStream( fileDir, FileMode.Create );
            inputImage.Save( fstream, System.Drawing.Imaging.ImageFormat.Jpeg );
            fstream.Close();

            savingTimes++;
           
            if(savingTimes != 5 )
                return;
            myTimer.Stop();
            finishMethod(true);
        }

        public void Finish(){
            appClosing( null, null );
        }

        private void appClosing(Object o, EventArgs args){
            
            if( myTimer != null )
                myTimer.Stop();
            if( dirChooseThread != null )
                dirChooseThread.Abort();
        }
    }
}
