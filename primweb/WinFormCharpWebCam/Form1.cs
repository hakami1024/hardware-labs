﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace WinFormCharpWebCam
{
    public partial class MainWinForm : Form
    {
        WebCam webcam;

        readonly Dictionary<String, int> santisecs = new Dictionary<string, int>{
                {"milliseconds", 1},
                {"seconds", 1000},
                {"minutes", 60000}
            };

        public MainWinForm()
        {
            InitializeComponent();
        }

        private void mainWinForm_Load(object sender, EventArgs e)
        {
            webcam = new WebCam();
            webcam.InitializeWebCam( ref imgVideo );

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            String secondsString = nUDTimeValue.Text;
            int timeValue = int.Parse( secondsString );
            int timeSign = santisecs[ dUDTimeSign.Text ];
            int millis = timeValue*timeSign;
            btnStart.Enabled = false;
            webcam.Start( millis, canCapture );
        }

        private void canCapture(bool result){
            Invoke( (Action)( () => {
                btnStart.Enabled = true;
            }) );
        }

        private void dUDTimeSign_SelectedItemChanged( object sender, EventArgs e ){
            this.nUDTimeValue.Increment = this.dUDTimeSign.Text == "milliseconds" ? 10 : 1;
        }

        protected override void OnFormClosing( FormClosingEventArgs e ){
            webcam.Finish();
        }
    }
}
